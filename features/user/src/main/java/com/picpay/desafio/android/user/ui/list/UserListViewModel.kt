package com.picpay.desafio.android.user.ui.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.picpay.desafio.android.common.PicPayResult
import com.picpay.desafio.android.user.domain.repository.UserRepository
import kotlinx.coroutines.launch

class UserListViewModel(private val userRepository: UserRepository) : ViewModel() {

    private val _stateMachine = MutableLiveData<UserListStateMachine>()
    val stateMachine: LiveData<UserListStateMachine>
        get() = _stateMachine

    fun getUserList() {
        viewModelScope.launch {
            _stateMachine.postValue(UserListStateMachine.Loading)

            fetchFromNetwork()
        }
    }

    private fun fetchFromNetwork() {
        viewModelScope.launch {
            when (val result = userRepository.fetchFromNetwork()) {
                is PicPayResult.Success -> {
                    _stateMachine.postValue(UserListStateMachine.SuccessRemote(result.value))
                }
                is PicPayResult.Error -> {
                    _stateMachine.postValue(UserListStateMachine.Error(result.value))
                }
            }.also {
                loadFromDb()
            }
        }
    }

    private fun loadFromDb() {
        viewModelScope.launch {
            when (val result = userRepository.loadFromDb()) {
                is PicPayResult.Success -> {
                    _stateMachine.postValue(UserListStateMachine.SuccessLocal(result.value))
                }
                is PicPayResult.Error -> {
                    _stateMachine.postValue(UserListStateMachine.Error(result.value))
                }
            }
        }
    }
}
