package com.picpay.desafio.android.user.ui.list

import com.picpay.desafio.android.common.PicPayError
import com.picpay.desafio.android.user.domain.model.UserModel

sealed class UserListStateMachine {
    object Loading : UserListStateMachine()
    class SuccessRemote(val updated: Boolean) : UserListStateMachine()
    class SuccessLocal(val userList: List<UserModel>) : UserListStateMachine()
    class Error(val error: PicPayError) : UserListStateMachine()
}
