package com.picpay.desafio.android.user.data.network

import com.picpay.desafio.android.user.data.network.response.UserResponse
import retrofit2.http.GET

interface UserService {

    @GET("users")
    suspend fun getUserList(): List<UserResponse>
}