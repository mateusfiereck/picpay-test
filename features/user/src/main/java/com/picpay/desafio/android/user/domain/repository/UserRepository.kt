package com.picpay.desafio.android.user.domain.repository

import com.picpay.desafio.android.common.PicPayResult
import com.picpay.desafio.android.common.PicPayError
import com.picpay.desafio.android.user.data.database.entity.UserEntity
import com.picpay.desafio.android.user.domain.model.UserModel

interface UserRepository {

    fun isNecessaryToUpdate(): Boolean
    suspend fun updateList(userList: List<UserEntity>)
    suspend fun fetchFromNetwork(): PicPayResult<Boolean, PicPayError>
    suspend fun loadFromDb(): PicPayResult<List<UserModel>, PicPayError>
}
