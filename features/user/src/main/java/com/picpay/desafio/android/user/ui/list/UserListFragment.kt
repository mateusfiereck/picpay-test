package com.picpay.desafio.android.user.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.picpay.desafio.android.common.PicPayError
import com.picpay.desafio.android.common.R
import com.picpay.desafio.android.navigation.NavRoutes
import com.picpay.desafio.android.navigation.Navigator
import com.picpay.desafio.android.user.databinding.UserListFragmentBinding
import com.picpay.desafio.android.user.domain.model.UserModel
import com.picpay.desafio.android.user.ui.list.adapter.UserListAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel
import com.picpay.desafio.android.user.ui.list.UserListStateMachine as StateMachine

class UserListFragment : Fragment() {

    private lateinit var binding: UserListFragmentBinding
    private val viewModel: UserListViewModel by viewModel()

    private val userListAdapter by lazy { UserListAdapter(::onClickItem) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = UserListFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupAdapter()
        setupObservers()

        if (savedInstanceState == null)
            viewModel.getUserList()
    }

    private fun setupAdapter() {
        binding.recyclerView.adapter = userListAdapter
    }

    private fun setupObservers() {
        viewModel.stateMachine.observe(viewLifecycleOwner, {
            when (it) {
                is StateMachine.Loading -> handleLoading()
                is StateMachine.SuccessLocal -> handleSuccessLocal(it.userList)
                is StateMachine.SuccessRemote -> handleSuccessRemote(it.updated)
                is StateMachine.Error -> handleError(it.error)
            }
        })
    }

    private fun handleLoading() {
        with(binding) {
            userListProgressBar.isVisible = true
            recyclerView.isVisible = false
        }
    }

    private fun handleSuccessRemote(updated: Boolean) {
        if (updated) {
            Toast.makeText(requireContext(), R.string.text_contacts_update, Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun handleSuccessLocal(userList: List<UserModel>) {
        with(binding) {
            userListAdapter.users = userList

            userListProgressBar.isVisible = false
            recyclerView.isVisible = true
        }
    }

    private fun handleError(error: PicPayError) {
        val message =
            if (error is PicPayError.NetworkError && error.isConnectionError) R.string.text_error_internet
            else R.string.text_error_generic

        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()

        with(binding) {
            userListProgressBar.isVisible = false
            recyclerView.isVisible = false
        }
    }

    private fun onClickItem(userModel: UserModel) {
        val bundle = bundleOf("user" to userModel)
        Navigator.goTo(NavRoutes.USER_DETAIL_FRAGMENT, findNavController(), bundle)
    }
}
