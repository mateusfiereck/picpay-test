package com.picpay.desafio.android.user.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.picpay.desafio.android.user.data.database.dao.UserDao
import com.picpay.desafio.android.user.data.database.entity.UserEntity

/*
* TODO
* Problema: Existe um banco de dados por feature.
* Solução: Extrair banco de dados para um módulo dentro de libraries.
*
* Novo problema: Entities precisam ser compartilhadas.
* Possível solução: Criar módulo público para cada feature: user-publi ou user-domain.
*/
@Database(entities = [UserEntity::class], version = 1, exportSchema = false)
abstract class UserRoomDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

    companion object {
        @Volatile
        private var INSTANCE: UserRoomDatabase? = null

        fun getInstance(context: Context): UserRoomDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext,
            UserRoomDatabase::class.java, "user_database"
        ).build()
    }
}