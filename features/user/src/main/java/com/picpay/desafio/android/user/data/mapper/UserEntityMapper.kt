package com.picpay.desafio.android.user.data.mapper

import com.picpay.desafio.android.user.data.database.entity.UserEntity
import com.picpay.desafio.android.user.data.network.response.UserResponse

class UserEntityMapper {

    fun mapFrom(from: List<UserResponse>): List<UserEntity> {
        return from.map {
            UserEntity(img = it.img, name = it.name, id = it.id, username = it.username)
        }
    }
}
