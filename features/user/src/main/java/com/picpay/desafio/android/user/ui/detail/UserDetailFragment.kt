package com.picpay.desafio.android.user.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.picpay.desafio.android.user.databinding.UserDetailFragmentBinding
import com.picpay.desafio.android.user.domain.model.UserModel

class UserDetailFragment : Fragment() {

    private lateinit var binding: UserDetailFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = UserDetailFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val user: UserModel? = arguments?.getParcelable<UserModel>("user")
        binding.textName.text = user?.name
    }
}
