package com.picpay.desafio.android.user.data.database.dao

import androidx.room.*
import com.picpay.desafio.android.user.data.database.entity.UserEntity

@Dao
interface UserDao {

    @Query("SELECT * from user_table")
    suspend fun getUserList(): List<UserEntity>

    @Query("DELETE from user_table")
    fun deleteUserTable()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUserList(userList: List<UserEntity>)
}
