package com.picpay.desafio.android.user.di

import android.content.Context
import com.picpay.desafio.android.user.data.database.UserRoomDatabase
import com.picpay.desafio.android.user.data.mapper.UserEntityMapper
import com.picpay.desafio.android.user.data.mapper.UserModelMapper
import com.picpay.desafio.android.user.data.network.UserService
import com.picpay.desafio.android.user.data.repository.UserRepositoryImpl
import com.picpay.desafio.android.user.domain.repository.UserRepository
import com.picpay.desafio.android.user.ui.list.UserListViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val userModule = module {
    // TODO extrair para um library module de persistencia
    single { UserRoomDatabase.getInstance(get()) }
    single { get<UserRoomDatabase>().userDao() }
    single { androidApplication().getSharedPreferences("preferences_picpay", Context.MODE_PRIVATE) }

    single { get<Retrofit>().create(UserService::class.java) }

    single<UserRepository> { UserRepositoryImpl(get(), get(), get(), get(), get()) }
    single { UserModelMapper() }
    single { UserEntityMapper() }

    viewModel { UserListViewModel(get()) }
}
