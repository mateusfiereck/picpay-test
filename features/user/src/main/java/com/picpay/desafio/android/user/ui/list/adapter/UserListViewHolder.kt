package com.picpay.desafio.android.user.ui.list.adapter

import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.picpay.desafio.android.user.R
import com.picpay.desafio.android.user.databinding.UserListItemBinding
import com.picpay.desafio.android.user.domain.model.UserModel
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

class UserListViewHolder(
    private val binding: UserListItemBinding,
    private val onClick: (UserModel) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(userModel: UserModel) {
        binding.name.text = userModel.name
        binding.username.text = userModel.username
        binding.progressBar.isVisible = true

        binding.root.setOnClickListener {
            onClick(userModel)
        }

        if (userModel.img.isNotEmpty()) {
            Picasso.get()
                .load(userModel.img)
                .error(R.drawable.ic_round_account_circle)
                .into(binding.picture, object : Callback {
                    override fun onSuccess() {
                        binding.progressBar.isVisible = false
                    }

                    override fun onError(e: Exception?) {
                        binding.progressBar.isVisible = false
                    }
                })
        }
    }
}