package com.picpay.desafio.android.user.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserModel(
    val img: String,
    val name: String,
    val id: Int,
    val username: String
) : Parcelable
