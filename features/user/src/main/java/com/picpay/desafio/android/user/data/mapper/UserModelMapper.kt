package com.picpay.desafio.android.user.data.mapper

import com.picpay.desafio.android.user.data.database.entity.UserEntity
import com.picpay.desafio.android.user.data.network.response.UserResponse
import com.picpay.desafio.android.user.domain.model.UserModel

class UserModelMapper {

    fun mapFromResponse(from: List<UserResponse>): List<UserModel> {
        return from.map {
            UserModel(img = it.img, name = it.name, id = it.id, username = it.username)
        }
    }

    fun mapFromEntity(from: List<UserEntity>): List<UserModel> {
        return from.map {
            UserModel(img = it.img, name = it.name, id = it.id, username = it.username)
        }
    }
}
