package com.picpay.desafio.android.user.data.repository

import android.content.SharedPreferences
import com.picpay.desafio.android.common.PicPayError
import com.picpay.desafio.android.common.PicPayResult
import com.picpay.desafio.android.user.data.database.dao.UserDao
import com.picpay.desafio.android.user.data.database.entity.UserEntity
import com.picpay.desafio.android.user.data.mapper.UserEntityMapper
import com.picpay.desafio.android.user.data.mapper.UserModelMapper
import com.picpay.desafio.android.user.data.network.UserService
import com.picpay.desafio.android.user.domain.model.UserModel
import com.picpay.desafio.android.user.domain.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.net.ConnectException
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit

class UserRepositoryImpl(
    private val userService: UserService,
    private val userDao: UserDao,
    private val userEntityMapper: UserEntityMapper,
    private val userModelMapper: UserModelMapper,
    private val sharedPreferences: SharedPreferences
) : UserRepository {

    companion object {
        private const val PREFERENCES_LAST_UPDATE = "PREFERENCES_LAST_UPDATE_IN_MILLIS"
        private val FRESH_TIMEOUT = TimeUnit.HOURS.toMillis(1)
    }

    override fun isNecessaryToUpdate(): Boolean {
        val currentTimeMillis = System.currentTimeMillis()
        val lastUpdate = sharedPreferences.getLong(PREFERENCES_LAST_UPDATE, 0)
        return if (currentTimeMillis > lastUpdate + FRESH_TIMEOUT) {
            sharedPreferences
                .edit()
                .putLong(PREFERENCES_LAST_UPDATE, currentTimeMillis)
                .apply()

            true
        } else {
            false
        }
    }

    override suspend fun updateList(userList: List<UserEntity>) {
        userDao.deleteUserTable()
        userDao.insertUserList(userList)
    }

    override suspend fun fetchFromNetwork(): PicPayResult<Boolean, PicPayError> {
        return withContext(Dispatchers.IO) {
            try {
                if (isNecessaryToUpdate()) {
                    updateList(userEntityMapper.mapFrom(userService.getUserList()))
                    PicPayResult.Success(true)
                } else {
                    PicPayResult.Success(false)
                }
            } catch (exception: Exception) {
                // TODO extrair para uma library de extensions ou common
                val error = when (exception) {
                    is HttpException -> {
                        PicPayError.NetworkError(
                            exceptionMessage = exception.message(),
                            isConnectionError = false
                        )
                    }
                    is ConnectException, is UnknownHostException -> {
                        PicPayError.NetworkError(
                            exceptionMessage = exception.message,
                            isConnectionError = true
                        )
                    }
                    else -> {
                        PicPayError.UnknownError(exception.message)
                    }
                }
                PicPayResult.Error(error)
            }
        }
    }

    override suspend fun loadFromDb(): PicPayResult<List<UserModel>, PicPayError> {
        return withContext(Dispatchers.IO) {
            try {
                PicPayResult.Success(userModelMapper.mapFromEntity(userDao.getUserList()))
            } catch (exception: Exception) {
                PicPayResult.Error(PicPayError.DatabaseError())
            }
        }
    }
}
