package com.picpay.desafio.android.user.data.service

import com.picpay.desafio.android.user.data.network.UserService
import com.picpay.desafio.android.user.data.network.response.UserResponse
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

class UserServiceTest {

    private val userService = mockk<UserService>()

    @Test
    fun test_getUserList() {
        val userListMock = emptyList<UserResponse>()
        coEvery {
            userService.getUserList()
        } returns userListMock

        runBlocking {
            val userList = userService.getUserList()

            Assert.assertEquals(userList, userListMock)
        }
    }
}
