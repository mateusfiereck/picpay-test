package com.picpay.desafio.android.user.data.mapper

import com.picpay.desafio.android.user.data.network.response.UserResponse
import com.picpay.desafio.android.user.domain.model.UserModel
import org.junit.Assert
import org.junit.Test

class UserModelMapperTest {

    private val userListMapper by lazy { UserModelMapper() }

    private val userListModel = listOf(
        UserModel(img = "", name = "Mateus Fiereck", id = 1, username = "mateusfiereck"),
        UserModel(img = "", name = "PicPay", id = 2, username = "picpay")
    )

    private val userListResponse = listOf(
        UserResponse(img = "", name = "Mateus Fiereck", id = 1, username = "mateusfiereck"),
        UserResponse(img = "", name = "PicPay", id = 2, username = "picpay")
    )

    @Test
    fun `when call mapFrom verify if was mapper work`() {
        val result = userListMapper.mapFromResponse(userListResponse)
        Assert.assertEquals(result, userListModel)
    }

}