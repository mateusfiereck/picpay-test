package com.picpay.desafio.android.user.ui.list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.picpay.desafio.android.common.PicPayError
import com.picpay.desafio.android.common.PicPayResult
import com.picpay.desafio.android.user.domain.model.UserModel
import com.picpay.desafio.android.user.domain.repository.UserRepository
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.*
import org.junit.Assert.assertTrue

@ExperimentalCoroutinesApi
class UserListViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    var testDispatcher = TestCoroutineDispatcher()

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun clearTests() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    private val userRepository = mockk<UserRepository>()
    private val viewModel by lazy { UserListViewModel(userRepository) }

    private val userListModel = listOf(
        UserModel(img = "", name = "Mateus Fiereck", id = 1, username = "mateusfiereck"),
        UserModel(img = "", name = "PicPay", id = 2, username = "picpay")
    )

    @Test
    fun `when fetchFromNetwork is called, state should be Success`() {
        coEvery { userRepository.fetchFromNetwork() } returns PicPayResult.Success(true)
        coEvery { userRepository.loadFromDb() } returns PicPayResult.Success(emptyList())

        viewModel.getUserList()

        assertTrue(viewModel.stateMachine.value is UserListStateMachine.SuccessLocal)
    }

    @Test
    fun `when fetchFromNetwork is called, state should be Error`() {
        coEvery { userRepository.fetchFromNetwork() } returns PicPayResult.Error(
            PicPayError.UnknownError("")
        )

        viewModel.getUserList()
        assertTrue(viewModel.stateMachine.value is UserListStateMachine.Error)
    }
}