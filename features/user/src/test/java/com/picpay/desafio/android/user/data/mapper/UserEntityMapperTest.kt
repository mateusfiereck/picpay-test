package com.picpay.desafio.android.user.data.mapper

import com.picpay.desafio.android.user.data.database.entity.UserEntity
import com.picpay.desafio.android.user.data.network.response.UserResponse
import com.picpay.desafio.android.user.domain.model.UserModel
import org.junit.Assert
import org.junit.Test

class UserEntityMapperTest {

    private val userListMapper by lazy { UserEntityMapper() }

    private val userListEntity = listOf(
        UserEntity(img = "", name = "Mateus Fiereck", id = 1, username = "mateusfiereck"),
        UserEntity(img = "", name = "PicPay", id = 2, username = "picpay")
    )

    private val userListResponse = listOf(
        UserResponse(img = "", name = "Mateus Fiereck", id = 1, username = "mateusfiereck"),
        UserResponse(img = "", name = "PicPay", id = 2, username = "picpay")
    )

    @Test
    fun `when call mapFrom verify if was mapper work`() {
        val result = userListMapper.mapFrom(userListResponse)
        Assert.assertEquals(result, userListEntity)
    }

}