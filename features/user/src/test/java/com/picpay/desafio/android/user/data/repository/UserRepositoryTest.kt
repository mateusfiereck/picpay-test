package com.picpay.desafio.android.user.data.repository

import android.content.SharedPreferences
import com.picpay.desafio.android.common.PicPayResult
import com.picpay.desafio.android.user.data.database.dao.UserDao
import com.picpay.desafio.android.user.data.database.entity.UserEntity
import com.picpay.desafio.android.user.data.mapper.UserEntityMapper
import com.picpay.desafio.android.user.data.mapper.UserModelMapper
import com.picpay.desafio.android.user.data.network.UserService
import com.picpay.desafio.android.user.data.network.response.UserResponse
import com.picpay.desafio.android.user.domain.model.UserModel
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class UserRepositoryTest {

    private val userService = mockk<UserService>()
    private val userDao = mockk<UserDao>()
    private val userEntityMapper = mockk<UserEntityMapper>()
    private val userModelMapper = mockk<UserModelMapper>()
    private val sharedPreferences = mockk<SharedPreferences>()

    private val userRepository by lazy {
        UserRepositoryImpl(
            userService = userService,
            userDao = userDao,
            userEntityMapper = userEntityMapper,
            userModelMapper = userModelMapper,
            sharedPreferences = sharedPreferences
        )
    }

    private val userListModel = listOf(
        UserModel(img = "", name = "Mateus Fiereck", id = 1, username = "mateusfiereck"),
        UserModel(img = "", name = "PicPay", id = 2, username = "picpay")
    )

    private val userListResponse = listOf(
        UserResponse(img = "", name = "Mateus Fiereck", id = 1, username = "mateusfiereck"),
        UserResponse(img = "", name = "PicPay", id = 2, username = "picpay")
    )

    private val userListEntity = listOf(
        UserEntity(img = "", name = "Mateus Fiereck", id = 1, username = "mateusfiereck"),
        UserEntity(img = "", name = "PicPay", id = 2, username = "picpay")
    )

    @Test
    fun `when call fetchFromNetwork verify if was called on success`() {
        coEvery { userRepository.isNecessaryToUpdate() } returns true
        coEvery { userRepository.updateList(any()) } returns Unit
        coEvery { userService.getUserList() } returns userListResponse
        every { userEntityMapper.mapFrom(any()) } returns userListEntity

        runBlocking {
            val result = userRepository.fetchFromNetwork()
            assertTrue(result is PicPayResult.Success)
            result as PicPayResult.Success
            assertEquals(result.value, true)
        }
    }

    @Test
    fun `when call loadFromDb verify if was called on success`() {
        every { userModelMapper.mapFromEntity(any()) } returns userListModel
        coEvery { userDao.getUserList() } returns userListEntity

        runBlocking {
            val result = userRepository.loadFromDb()
            assertTrue(result is PicPayResult.Success)
            result as PicPayResult.Success
            assertEquals(result.value, userListModel)
        }
    }
}
