package com.picpay.desafio.android.user.list

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.picpay.desafio.android.common.PicPayResult
import com.picpay.desafio.android.user.R
import com.picpay.desafio.android.user.RecyclerViewMatchers
import com.picpay.desafio.android.user.domain.model.UserModel
import com.picpay.desafio.android.user.domain.repository.UserRepository
import com.picpay.desafio.android.user.ui.list.UserListFragment
import com.picpay.desafio.android.user.ui.list.UserListViewModel
import io.mockk.coEvery
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

fun prepareUserListFragment(func: UserListFragmentRobot.() -> Unit): UserListFragmentRobot {
    return UserListFragmentRobot().apply(func)
}

@KoinApiExtension
class UserListFragmentRobot : KoinComponent {

    private val userRepository: UserRepository by inject()
    private var userListViewModel: UserListViewModel

    private val userListModel = listOf(
        UserModel(img = "", name = "Mateus Fiereck", id = 1, username = "mateusfiereck"),
        UserModel(img = "", name = "PicPay", id = 2, username = "picpay")
    )

    init {
        userListViewModel = UserListViewModel(userRepository)
        loadKoinModules(module(override = true) { single { userListViewModel } })
    }

    fun mockGetUserList() {
        coEvery { userRepository.fetchFromNetwork() } returns PicPayResult.Success(false)
        coEvery { userRepository.loadFromDb() } returns PicPayResult.Success(userListModel)
    }

    infix fun launchUserListFragment(func: UserListFragmentRobot.() -> Unit): UserListFragmentRobot {
        launchFragmentInContainer<UserListFragment>(
            themeResId = R.style.Theme_PicPay_Light
        )
        return UserListFragmentRobot().apply(func)
    }

    infix fun check(func: UserListFragmentResult.() -> Unit): UserListFragmentResult {
        return UserListFragmentResult().apply(func)
    }

    class UserListFragmentResult {
        fun displayTitle() {
            Espresso.onView(ViewMatchers.withText("Contatos"))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        }

        fun displayList() {
            RecyclerViewMatchers.checkRecyclerViewItem(
                R.id.recyclerView,
                0,
                ViewMatchers.withText("Mateus Fiereck")
            )
        }
    }
}