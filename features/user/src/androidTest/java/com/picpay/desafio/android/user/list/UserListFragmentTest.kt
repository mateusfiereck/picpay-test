package com.picpay.desafio.android.user.list

import androidx.test.espresso.intent.Intents
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.picpay.desafio.android.network.di.networkModule
import com.picpay.desafio.android.user.data.database.dao.UserDao
import com.picpay.desafio.android.user.di.userModule
import com.picpay.desafio.android.user.domain.repository.UserRepository
import io.mockk.mockk
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.component.KoinApiExtension
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.KoinTestRule

@KoinApiExtension
@RunWith(AndroidJUnit4ClassRunner::class)
class UserListFragmentTest : KoinTest {

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        modules(userModule + networkModule + mockedRepositories)
    }

    private val mockedRepositories = module {
        single(override = true) { mockk<UserRepository>(relaxed = true) }
    }

    @Before
    fun setup() = Intents.init()

    @After
    fun release() = Intents.release()

    @Test
    fun shouldDisplayTitle() {
        prepareUserListFragment {
            mockGetUserList()
        } launchUserListFragment {
        } check {
            displayTitle()
        }
    }

    @Test
    fun shouldDisplayListItem() {
        prepareUserListFragment {
            mockGetUserList()
        } launchUserListFragment {

        } check {
            displayList()
        }
    }
}