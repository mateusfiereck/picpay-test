package com.picpay.desafio.android

import android.app.Application
import com.picpay.desafio.android.network.di.networkModule
import com.picpay.desafio.android.user.di.userModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

open class PicPayApp : Application() {

    override fun onCreate() {
        super.onCreate()

        setupKoin()
    }

    private fun setupKoin() {
        startKoin {
            androidContext(this@PicPayApp)
            modules(userModule, networkModule)
        }
    }
}
