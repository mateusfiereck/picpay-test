package com.picpay.desafio.android.navigation

import android.net.Uri
import android.os.Bundle
import androidx.navigation.NavController

object Navigator {

    fun goTo(deepLink: String, navController: NavController,  bundle: Bundle? = null) {
        val deepLinkUri = Uri.parse("app://picpay/$deepLink")
        val navDestination = navController.graph.find {
            it.hasDeepLink(deepLinkUri)
        }
        navDestination?.let {
            navController.navigate(navDestination.id, bundle)
        }
    }
}