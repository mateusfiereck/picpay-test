package com.picpay.desafio.android.common

sealed class PicPayError(val message: String?) {
    class NetworkError(
        val exceptionMessage: String?,
        val isConnectionError: Boolean
    ) : PicPayError(exceptionMessage)

    class DatabaseError() : PicPayError("Database error")
    class UnknownError(val exceptionMessage: String?) : PicPayError(exceptionMessage)
}
