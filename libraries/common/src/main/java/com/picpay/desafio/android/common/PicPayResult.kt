package com.picpay.desafio.android.common

sealed class PicPayResult<out D, out E> {
    data class Success<D>(val value: D) : PicPayResult<D, Nothing>()
    data class Error<E>(val value: E) : PicPayResult<Nothing, E>()
}
