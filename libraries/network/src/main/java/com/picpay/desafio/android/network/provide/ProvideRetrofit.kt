package com.picpay.desafio.android.network.provide

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ProvideRetrofit {

    private const val BASE_URL = "http://careers.picpay.com/tests/mobdev/"

    fun build(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}
