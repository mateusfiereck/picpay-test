package com.picpay.desafio.android.network.provide

import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

object ProvideOkHttp {

    fun build(): OkHttpClient {
        val builder = OkHttpClient.Builder()

        builder.readTimeout(15, TimeUnit.SECONDS)
        builder.writeTimeout(15, TimeUnit.SECONDS)
        builder.connectTimeout(15, TimeUnit.SECONDS)

        return builder.build()
    }
}
