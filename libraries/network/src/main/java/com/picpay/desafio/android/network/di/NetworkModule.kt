package com.picpay.desafio.android.network.di

import com.picpay.desafio.android.network.provide.ProvideOkHttp
import com.picpay.desafio.android.network.provide.ProvideRetrofit
import org.koin.dsl.module

val networkModule = module {
    single { ProvideOkHttp.build() }
    single { ProvideRetrofit.build(get()) }
}
